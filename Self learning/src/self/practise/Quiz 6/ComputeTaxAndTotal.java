package self.practise;

import java.util.Scanner;

public class ComputeTaxAndTotal {
	
	public static void main(String[] args) {
		Scanner in = new Scanner (System.in);
		System.out.print("Enter the total :");
		int total = in.nextInt();
		
		Scanner scan = new Scanner (System.in);
		System.out.print("Enter the tax rate :");
		double taxPercent = scan.nextDouble();
		int tax = (int) (total * 0.01 * taxPercent);
		int subtotal = total - tax;
		
		System.out.println("The subtotal = "+subtotal/100+" dollars and "+subtotal%100+" cents.");
		System.out.println("The tax rate = "+ taxPercent +" percent.");
		System.out.println("The tax = "+tax/100+" dollars and "+tax%100+" cents.");
		System.out.println("The total = "+ total/100+" dollars and "+ total%100+" cents.");
	}
}

/*
Enter the total :10000
Enter the tax rate :5.5
The subtotal = 94 dollars and 50 cents.
The tax rate = 5.5 percent.
The tax = 5 dollars and 50 cents.
The total = 100 dollars and 0 cents.

*/