package self.practise;

import java.util.Scanner;

public class IsRightAngleTriangle {
	public static void main(String[] args){
		
		  Scanner in = new Scanner(System.in);
		  System.out.print(" sideA : ");
		  double A = in .nextDouble();
		  System.out.print(" sideB : ");
		  double B = in .nextDouble();
		  System.out.print(" sideC : ");
		  double C = in .nextDouble();

		  	System.out.print("Is Right Angle Triangle: " + isRightAngleTriangle(A, B, C));
		}

	   public static boolean isRightAngleTriangle(double A, double B, double C) {
	        if((Math.pow(A,2)+Math.pow(B,2)) == Math.pow(C,2))
	            return true;
	        if((Math.pow(B,2)+Math.pow(C,2)) == Math.pow(B,2))
	            return true;
	        if((Math.pow(C,2)+Math.pow(B,2)) == Math.pow(A,2))
	            return true;
	        else
	            return false;
	    }
}

