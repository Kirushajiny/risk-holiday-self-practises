package self.practise;

public class TriangleValidity {
	public static void main(String[] args) {
		
		double sideA=3; double sideB=5; double sideC=8;

		boolean status= isValidTriangle(sideA, sideB, sideC);
		
		if(status) {
			System.out.println("ABC is a valid triangle");
		}else {
			System.out.println("ABC is not a valid triangle");
		}
	}

	public static boolean isValidTriangle(double sideA, double sideB, double sideC) {

		double ab = sideA + sideB;
		double ac = sideA + sideC;
		double bc = sideB + sideC;

		if ((sideA < bc) && (sideB < ac) && (sideC < ab)) {
			return true;
		}
		return false;
	}
}

