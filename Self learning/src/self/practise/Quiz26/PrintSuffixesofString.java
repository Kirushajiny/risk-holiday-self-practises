package self.practise.Quiz26;

public class PrintSuffixesofString {
	
	public static void main(String[] args) {
		
		String word = "hurricanes";
		int length = word.length();
		
		for (int coloumn = 0; coloumn <= length; coloumn++) {
			for (int i = coloumn; i <= coloumn; i++) {
				System.out.print(word.substring(i));
			}
			System.out.println();
		}
	}
}

/*
hurricanes
urricanes
rricanes
ricanes
icanes
canes
anes
nes
es
s
*/