package self.practise.Quiz28;
import java.util.Scanner;

public class StringReverse {
	
	public static void main(String[] args) {
		
		Scanner scan = new Scanner(System.in);
		System.out.print("Enter an input String : ");
		
		String s = scan.nextLine();
		String reverse = " ";
		
		int length = s.length();
		for (int i = length - 1; i >= 0; i--) {
			reverse = reverse + s.charAt(i);
		}
		System.out.println("The reverse of "  + s +  " is "  + reverse + " ");
	}
}

/*
 Enter an input String : Computer-Programming
The reverse of Computer-Programming is  gnimmargorP-retupmoC 

 */

