package self.practise.Quiz21;

import java.util.Scanner;

	public class ReceiveAndPrint {
		public static void main(String args[]) {
				Scanner scan = new Scanner(System.in);
				
				System.out.print("ENTER STRING :");
				String s = scan.nextLine();
				System.out.print("ENTER INTEGER : ");
				int m = scan.nextInt();
				System.out.print("ENTER DOUBLE : ");
				double d = scan.nextDouble();
				
				System.out.println("S: " +s);
				System.out.println("M: " +m);
				System.out.println("D: " +d);
				System.out.println("M+D+S: " +m+d+s);
				System.out.println("M+S+D: " +m+s+d);
				System.out.println("S+M+D: " +s+m+d);
			}
		}

/*
	ENTER STRING : Kirushajiny
	ENTER INTEGER : 6
	ENTER DOUBLE : 3.5
	S:  Kirushajiny
	M: 6
	D: 3.5
	M+D+S: 63.5 Kirushajiny
	M+S+D: 6 Kirushajiny3.5
	S+M+D:  Kirushajiny63.5
*/
	