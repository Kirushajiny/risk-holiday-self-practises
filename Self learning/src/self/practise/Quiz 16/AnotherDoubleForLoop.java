package self.practise;

public class AnotherDoubleForLoop {

   public static void main(String[] args) {
	
	int num = 10;
	
			for (int i = 0; i < num / 2; i++ ){ 
				for ( int m =(num - i - 1); m > i; m-- ){ 
					System.out.print( m ); 
				} 
					System.out.println(); 
			}
		}

}

/*
987654321
8765432
76543
654
5
*/