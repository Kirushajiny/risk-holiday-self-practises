package self.practise;
import java.util.Scanner;

public class PostiveValues {
		public static void main(String[] args){
			
			  Scanner in = new Scanner(System.in);
			  System.out.print(" ValueA : ");
			  double A = in .nextDouble();
			  System.out.print(" ValueB : ");
			  double B = in .nextDouble();
			  System.out.print(" ValueC : ");
			  double C = in .nextDouble();

			  	System.out.print("Are  all positive values : " + allPositive(A, B, C));
			}

		   public static boolean allPositive(double A, double B, double C) {
			   return(A>=0 && B>=0 && C>=0);
	   
		    }
	}

// Output

ValueA : 2
ValueB : 4
ValueC : 6
Are  all positive values : true


