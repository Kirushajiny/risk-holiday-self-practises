package self.practise;
import java.util.Scanner;

public class UserInput {

	private static Scanner scan = new Scanner(System.in);

	public static int readNumber(String msg) {
		System.out.print(msg + ": ");
		return scan.nextInt();
	}

	public static double readDecimal(String msg) {
		System.out.println(msg + ": ");
		return scan.nextDouble();
	}

	public static String readText(String msg) {
		System.out.print(msg + ": ");
		return scan.nextLine();
	}
}




